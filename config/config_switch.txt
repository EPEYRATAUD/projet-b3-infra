version 12.1
no service pad
service timestamps debug uptime
service timestamps log uptime
service password-encryption
!
hostname Switch
!
enable secret 5 $1$g7V2$01jHndWe7DiJjcFUIQKr41
!
username whs password 7 051C0E1C
ip subnet-zero
!
ip domain-name whs.local
ip ssh time-out 120
ip ssh authentication-retries 3
ip ssh version 2
!
spanning-tree mode pvst
no spanning-tree optimize bpdu transmission
spanning-tree extend system-id
!
!
!
!
interface FastEthernet0/1
 switchport access vlan 100
 switchport mode access
!
interface FastEthernet0/2
 switchport access vlan 100
 switchport mode access
!
interface FastEthernet0/3
 switchport access vlan 100
 switchport mode access
!
interface FastEthernet0/4
 switchport access vlan 100
 switchport mode access
!
interface FastEthernet0/5
 switchport access vlan 100
 switchport mode access
!
interface FastEthernet0/6
 switchport access vlan 99
 switchport mode access
!
interface FastEthernet0/7
!
interface FastEthernet0/8
!
interface FastEthernet0/9
!
interface FastEthernet0/10
!
interface FastEthernet0/11
!
interface FastEthernet0/12
!
interface FastEthernet0/13
!
interface FastEthernet0/14
!
interface FastEthernet0/15
!
interface FastEthernet0/16
!
interface FastEthernet0/17
!
interface FastEthernet0/18
!
interface FastEthernet0/19
!
interface FastEthernet0/20
!
interface FastEthernet0/21
!
interface FastEthernet0/22
!
interface FastEthernet0/23
!
interface FastEthernet0/24
!
interface GigabitEthernet0/1
 switchport access vlan 100
 switchport mode access
!
interface GigabitEthernet0/2
!
interface Vlan1
 no ip address
 no ip route-cache
 shutdown
!
interface Vlan99
 ip address 192.168.101.1 255.255.255.0
 no ip route-cache
 shutdown
!
interface Vlan100
 ip address 192.168.100.1 255.255.255.0
 no ip route-cache
!
ip http server
!
line con 0
 logging synchronous
 login local
line vty 0 4
 password 7 08314D5D1A0E0A0516
 login local
 transport input ssh
line vty 5 15
 login local
 transport input ssh
!
!
end