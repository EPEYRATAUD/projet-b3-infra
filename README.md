# Projet B3 Infra

# Sommaire

- [1. Equipements nécessaires pour la réalisation de notre infrastructure :](#1-equipements-nécessaires-pour-la-réalisation-de-notre-infrastructure)

- [2. Configuration des équipements réseaux :](#2-configuration-des-équipements-réseaux-)

- [3. Tableau des réseaux / informations de l'infrastructure](#3-tableau-des-réseaux-informations-de-linfrastructure)

- [4. Schéma / Photo de l'infrastructure](#4-schéma-photo-de-linfrastructure)

- [5. Installation d'un ESXi :](#5-installation-dun-serveur-esxi-65-)

- [6. Déploiement du vCenter :](#6-déploiement-du-vcenter-)

- [7. Mise en place d'un vSan et vMotion :](#7-mise-en-place-dun-vsan-et-vmotion-)

- [8. Déploiement d'une VM Windows Serveur depuis le vCenter :](#8-déploiement-dune-vm-windows-serveur-depuis-le-vcenter-)

- [9. Mise en place d'un Active Directory :](#9-mise-en-place-dun-active-directory-)

- [10. Mise en place du DHCP et DNS :](#10-mise-en-place-du-dhcp-et-dns-)

- [11. Mise en place le partage de fichiers SMB :](#11-mise-en-place-le-partage-de-fichiers-smb-)

## 1. Equipements nécessaires pour la réalisation de notre infrastructure :

- **3 serveurs Dell PowerEdge R610**
- **1 switch Cisco Catalyst 2650**
- **1 routeur Cisco ASA 5510**
- **`(Nous vous recommandons d'utiliser du matériel plus récent)`**

## 2. Configuration des équipements réseaux :

**Configuration du Switch :**

- 📃 Fichier **[config_switch.txt](/config/config_switch.txt)**

Voici les détails de la configuration du **Switch** :

- Les interfaces FastEthernet0/1 à FastEthernet0/5 et GigabitEthernet0/1 sont configurées en mode access et sont associées au VLAN 100.

- L'interface FastEthernet0/6 est configurée en mode access et est associée au VLAN 99.
- Les interfaces Vlan1 et Vlan99 sont désactivées (shutdown).

- L'interface Vlan100 a une adresse IP de 192.168.100.1 avec un masque de sous-réseau de 255.255.255.0.

- L'interface Vlan99 a une adresse IP de 192.168.101.1 avec un masque de sous-réseau de 255.255.255.0 (désactivée car on ne l'a pas utilisé).

- Des utilisateurs sont configurés avec des mots de passe chiffrés pour l'authentification SSH et la gestion du routeur.

**Configuration du Routeur :**

- 📃 Fichier **[config_cisco_ASA.txt](/config/config_cisco_ASA.txt)**

Voici les détails de la configuration du **Routeur** :

- L'interface Ethernet0/0 est configurée en tant qu'interface **"outside"** avec une adresse IP de 10.33.14.150 et un masque de sous-réseau de 255.255.255.0.

- L'interface Ethernet0/1 est configurée en tant qu'interface **"inside"** avec une adresse IP de 192.168.100.254 et un masque de sous-réseau de 255.255.255.0.

- L'interface Management0/0 est configurée en tant qu'interface **"management"** avec une adresse IP de 192.168.101.1 et un masque de sous-réseau de 255.255.255.0.

- Le routeur a la configuration du **NAT** (Network Address Translation) pour permettre la traduction d'adresses entre les réseaux internes et externes.

- Des listes d'accès sont configurées pour permettre le trafic **ICMP** (ping) entrant et sortant.

- Le routeur est configuré pour utiliser les serveurs **DNS** Google (8.8.8.8).

- Le service HTTP est activé et accessible à partir du réseau de gestion (192.168.101.0/24).

- Des utilisateurs sont configurés avec des mots de passe chiffrés pour l'authentification SSH et la gestion du routeur.

- Différentes politiques d'inspection sont configurées pour le trafic entrant et sortant.

## 3. Tableau des réseaux / informations de l'infrastructure

#### Routeur

| Adresse IP      | Interface |
| --------------- | --------- |
| 192.168.100.254 | LAN       |
| 10.33.14.150    | WAN       |

#### Switch

| Interface          | Mode   | VLAN |
| ------------------ | ------ | ---- |
| FastEthernet0/1    | access | 100  |
| FastEthernet0/2    | access | 100  |
| FastEthernet0/3    | access | 100  |
| FastEthernet0/4    | access | 100  |
| FastEthernet0/5    | access | 100  |
| GigabitEthernet0/1 | access | 100  |

#### WHS-ESX01

| Hôte        | URL                    |
| ----------- | ---------------------- |
| WHS-ESX01   | https://192.168.100.10 |
| Idrac-esx01 | https://192.168.100.11 |

#### WHS-ESX02

| Hôte        | URL                    |
| ----------- | ---------------------- |
| WHS-ESX02   | https://192.168.100.20 |
| Idrac-esx02 | https://192.168.100.21 |

#### WHS-ESX03

| Hôte        | URL                    |
| ----------- | ---------------------- |
| WHS-ESX03   | https://192.168.100.30 |
| Idrac-esx03 | https://192.168.100.31 |

#### vCenter

| Service    | URL                          |
| ---------- | ---------------------------- |
| Web        | https://192.168.100.100      |
| Web config | https://192.168.100.100:5480 |

#### Machines virtuelles

| Nom     | Adresse IP      |
| ------- | --------------- |
| WHS-AD  | 192.168.100.50  |
| WHS-ADM | 192.168.100.110 |
| WHS-DFS | 192.168.100.120 |

## 4. Schéma / Photo de l'infrastructure

### Photo de notre matériel :

![](pics/infra1.jpg)

![](pics/infra2.jpg)

### Schéma de notre infrastructure réseau :

![](pics/Schema-de-l'infra.png)

## 5. Installation d'un serveur Esxi 6.5 :

- Récupérer un iso **💿`VMware ESXi 6.5`**.

- Pour installer et configurer **`VMware ESXi 6.5`** sur un serveur Dell, suivez les étapes ci-dessous :

> **1** - Téléchargement de l'image ISO : Accédez au site Web de VMware et téléchargez l'image ISO d'ESXi 6.5.
>
> **2** - Préparation de la clé USB : Utilisez un logiciel de création de clé USB bootable, comme Rufus, pour créer une clé USB bootable avec l'image ISO d'ESXi 6.5.
>
> **3** - Configuration du serveur Dell : Assurez-vous que le serveur Dell est correctement configuré et prêt pour l'installation d'ESXi 6.5. Vérifiez que les composants matériels tels que les disques durs, la mémoire et les cartes réseau sont correctement installés et détectés.
>
> **4** - Démarrage du serveur : Insérez la clé USB bootable dans le serveur Dell et démarrez-le. Assurez-vous que le serveur est configuré pour démarrer à partir de la clé USB.
>
> **5** - Installation d'ESXi 6.5 : Lorsque le serveur démarre, il affiche un menu de démarrage. Sélectionnez l'option pour installer ESXi 6.5. Suivez les instructions à l'écran pour choisir la langue, accepter les conditions de licence, sélectionner le disque d'installation et configurer le mot de passe administrateur.
>
> **6** - Configuration du réseau : Après l'installation, configurez les paramètres réseau d'ESXi 6.5. Vous pouvez le faire en utilisant l'interface de ligne de commande ou l'interface graphique du serveur ESXi. Assurez-vous de configurer une adresse IP statique ou de configurer un serveur DHCP pour obtenir une adresse IP.
>
> **7** - Configuration des paramètres de stockage : Configurez les paramètres de stockage en ajoutant des disques durs, en créant des datastores et en configurant les options de stockage selon vos besoins.
>
> **8** - Configuration des autres paramètres : Configurez d'autres paramètres tels que la gestion à distance, la sécurité et les fonctionnalités avancées en fonction de vos besoins spécifiques.
>
> - Vérification de la configuration : Une fois la configuration terminée, assurez-vous de tester la connectivité réseau, la disponibilité du stockage et les autres fonctionnalités pour vous assurer que tout fonctionne comme prévu.

- 📺[Vidéo explicative](https://www.youtube.com/watch?v=tKlXg3IHpRM)
- 📺[Configuration](https://www.youtube.com/watch?v=IP0yOC5St6s)

## 6. Déploiement du vCenter :

- **Pour déployer vCenter Appliance 6.5 sur un hôte ESXi 6.5, suivez les étapes suivantes :**
  > **1** - Téléchargement de l'appliance vCenter : Accédez au site Web de VMware et téléchargez l'appliance vCenter 6.5 au format OVA.
  >
  > **2** - Connexion à vSphere Client : Lancez vSphere Client et connectez-vous à votre hôte ESXi 6.5.
  >
  > **3** - Déploiement de l'appliance vCenter : Dans le vSphere Client, accédez au menu "Fichier" et sélectionnez "Déployer un OVF/OVA". Parcourez et sélectionnez le fichier OVA de l'appliance vCenter que vous avez téléchargé.
  >
  > **4** - Spécification des paramètres de déploiement : Suivez les instructions à l'écran pour spécifier les paramètres de déploiement de l'appliance vCenter, tels que le nom de l'appliance, l'emplacement de déploiement, les options réseau, les paramètres de stockage, etc. Assurez-vous de configurer les paramètres appropriés en fonction de votre environnement.
  >
  > **5** - Acceptation des conditions de licence : Lisez les conditions de licence et acceptez-les pour poursuivre le déploiement.
  >
  > **6** - Sélection de la taille de déploiement : Choisissez la taille de déploiement de l'appliance vCenter en fonction de la taille de votre environnement. Les options disponibles peuvent varier en fonction de l'appliance téléchargée.
  >
  > **7** - Configuration du réseau : Configurez les paramètres réseau de l'appliance vCenter, tels que l'adresse IP, le masque de sous-réseau, la passerelle par défaut et les paramètres DNS. Assurez-vous de fournir des informations réseau valides pour que l'appliance puisse communiquer correctement dans votre environnement.
  >
  > **8** - Configuration de la base de données : Sélectionnez l'option de base de données intégrée ou externe, en fonction de vos besoins. Si vous choisissez la base de données intégrée, spécifiez les paramètres de base de données, tels que le nom d'utilisateur, le mot de passe et la taille de la base de données. Si vous choisissez une base de données externe, fournissez les informations de connexion appropriées.
  >
  > **9** - Configuration des autres paramètres : Configurez d'autres paramètres tels que le fuseau horaire, le mot de passe de l'utilisateur root, les paramètres NTP, etc.
  >
  > **10** - Validation des paramètres et déploiement : Passez en revue tous les paramètres de déploiement pour vous assurer qu'ils sont corrects. Une fois que tout est configuré comme souhaité, cliquez sur "Déployer" pour démarrer le processus de déploiement de l'appliance vCenter.
  >
  > **11** - Suivi du processus de déploiement : Suivez l'avancement du déploiement de l'appliance vCenter. Cela peut prendre quelques minutes.
  >
  > **12** - Configuration initiale de vCenter : Une fois le déploiement terminé, ouvrez un navigateur Web et accédez à l'URL spécifiée dans les informations de déploiement pour accéder à l'interface de configuration initiale de vCenter. Suivez les étapes à l'écran pour terminer la configuration initiale de vCenter, y compris la création d'un utilisateur administrateur.
  >
  > Une fois ces étapes terminées, vous aurez déployé vCenter Appliance 6.5 sur votre hôte ESXi 6.5. Vous pouvez ensuite utiliser vCenter pour gérer votre environnement virtualisé. N'oubliez pas de consulter la documentation de VMware pour obtenir des instructions plus détaillées si nécessaire.
- 📺[Vidéo explicative](https://www.youtube.com/watch?v=5pow0QMX1GI)

## 7. Mise en place d'un vSan et vMotion :

- **Pour mettre en place vSAN (Virtual SAN) et vMotion sur vCenter Appliance 6.5, suivez les étapes suivantes :**
- **Configuration de vSAN :**

> - Assurez-vous que tous les hôtes ESXi participant au cluster vSAN sont compatibles avec vSAN 6.5. Vérifiez la compatibilité matérielle sur le site Web de VMware.
> - Dans vCenter, accédez à l'inventaire et sélectionnez le cluster vSAN.Cliquez avec le bouton droit de la souris sur le cluster et choisissez "Activer/Configurer vSAN".
> - Suivez les instructions à l'écran pour configurer les paramètres de vSAN, tels que la sélection du mode de redondance (mirroring ou RAID-5/6), la création d'un pool de disques, l'allocation de capacité, etc.
> - Une fois la configuration terminée, vSAN sera activé sur le cluster et vous pourrez commencer à utiliser le stockage vSAN.

- **Configuration de vMotion :**

> - Assurez-vous que tous les hôtes ESXi participant au cluster vMotion sont connectés au même réseau vMotion et que les connexions réseau appropriées sont configurées.
> - Dans vCenter, accédez à l'inventaire et sélectionnez le cluster vMotion.
> - Cliquez avec le bouton droit de la souris sur le cluster et choisissez "Configurer" > "Paramètres".
> - Dans l'onglet "vMotion", configurez les paramètres de vMotion, tels que l'adresse IP du réseau vMotion, les paramètres de bande passante, etc.
> - Une fois la configuration terminée, vMotion sera activé sur le cluster et vous pourrez commencer à effectuer des migrations en direct des machines virtuelles entre les hôtes ESXi.
> - Assurez-vous de prendre en compte les prérequis matériels et de réseau appropriés pour vSAN et vMotion, ainsi que les bonnes pratiques recommandées par VMware. N'hésitez pas à consulter la documentation officielle de VMware pour obtenir des instructions détaillées et spécifiques à votre version de vCenter Appliance 6.5.

- **Pour la mise en place de vSan, suivre la documentation de** **[Vmware](https://docs.vmware.com/fr/search/#/vsan)**:
- **Pour la mise en place de vMotion, suivre la documentation de** **[Vmware](https://docs.vmware.com/fr/search/#/vmotion)**:

## 8. Déploiement d'une VM Windows Serveur depuis le vCenter :

- **Pour déployer une machine virtuelle (VM) Windows Server à partir de vCenter 6.5, vous pouvez suivre les étapes suivantes :**

> **Étape 1: Préparation des fichiers d'installation :**
>
> - Assurez-vous d'avoir le fichier d'installation ISO de Windows Server à disposition.
> - Copiez le fichier ISO de Windows Server sur le stockage accessible depuis le vCenter Server.
>
> **Étape 2: Création d'un modèle de VM personnalisé (Template) :**
>
> - Connectez-vous à vCenter Server à l'aide du client vSphere.
> - Cliquez avec le bouton droit de la souris sur l'hôte ou le cluster où vous souhaitez déployer la VM, puis sélectionnez "Nouvelle machine virtuelle" > "Créer un modèle virtuel".
> - Suivez les étapes de l'assistant pour créer le modèle de VM personnalisé en spécifiant le nom, l'emplacement et les ressources de la VM.
> - Lorsque vous arrivez à la page "Personnaliser les paramètres", sélectionnez la quantité de mémoire, le nombre de processeurs et d'autres paramètres appropriés pour votre environnement.
> - Sur la page "Sélectionner un disque", cliquez sur "Utiliser un fichier ISO distant" et spécifiez le chemin d'accès vers le fichier ISO de Windows Server que vous avez copié précédemment.
> - Continuez à suivre les étapes de l'assistant pour terminer la création du modèle de VM personnalisé.
>
> **Étape 3: Déploiement de la VM Windows Server :**
>
> - Dans le client vSphere, cliquez avec le bouton droit de la souris sur l'hôte ou le cluster où vous souhaitez déployer la VM, puis sélectionnez "Nouvelle machine virtuelle" > "Déployer à partir d'un modèle virtuel".
> - Suivez les étapes de l'assistant pour déployer la VM en spécifiant le nom, l'emplacement et les ressources de la VM.
> - Lorsque vous arrivez à la page "Personnaliser les paramètres", sélectionnez la quantité de mémoire, le nombre de processeurs et d'autres paramètres appropriés pour votre VM Windows Server.
> - Sur la page "Sélectionner un disque", assurez-vous de choisir le modèle de VM personnalisé que vous avez créé dans l'étape précédente.
> - Continuez à suivre les étapes de l'assistant pour terminer le déploiement de la VM.
> - Une fois le déploiement terminé, la VM Windows Server sera opérationnelle et vous pourrez vous y connecter à l'aide d'un client RDP (Remote Desktop Protocol) pour effectuer la configuration et l'installation du système d'exploitation.
>
> - N'oubliez pas de configurer les paramètres de réseau, les options de sécurité et les autres configurations nécessaires sur la VM Windows Server nouvellement déployée en fonction de vos besoins spécifiques

## 9. Mise en place d'un Active Directory :

- **Pour mettre en place un Active Directory sur Windows Server 2022, suivez les étapes suivantes :**

> **1** **-Installation du rôle Active Directory :**
>
> - Démarrez votre serveur Windows Server 2022 et connectez-vous en tant qu'administrateur.
> - Ouvrez le Gestionnaire de serveur en cliquant sur l'icône correspondante dans la barre des tâches ou en utilisant la fonction de recherche.
> - Dans le Gestionnaire de serveur, cliquez sur "Ajouter des rôles et des fonctionnalités".
> - Suivez les instructions de l'assistant d'installation pour sélectionner le serveur sur lequel vous souhaitez installer les rôles et les fonctionnalités.
> - Dans la liste des rôles disponibles, sélectionnez "Services de domaine Active Directory".
> - Acceptez les fonctionnalités supplémentaires recommandées pour le rôle Active Directory et cliquez sur "Installer" pour démarrer l'installation.
>
> **2** **- Promotion du serveur en contrôleur de domaine :**
>
> - Une fois l'installation du rôle Active Directory terminée, le Gestionnaire de serveur vous proposera de configurer le serveur en tant que contrôleur de domaine. Cliquez sur "Promouvoir ce serveur en contrôleur de domaine".
> - Sélectionnez l'option "Ajouter une nouvelle forêt" si vous souhaitez créer une nouvelle forêt Active Directory. Sinon, sélectionnez l'option appropriée en fonction de votre environnement.
> - Entrez un nom de domaine pour votre forêt Active Directory. Par exemple, "mondomaine.local".
> - Sélectionnez les options de niveau fonctionnel de la forêt et du domaine. Vous pouvez généralement utiliser les valeurs par défaut, sauf si vous avez des exigences spécifiques.
> - Configurez le mot de passe DSRM (mode de restauration des services d'annuaire) pour l'administrateur de votre domaine.
> - Spécifiez les emplacements des fichiers de base de données Active Directory et des journaux de transactions.
> - Sélectionnez les options de réplication DNS appropriées en fonction de votre infrastructure réseau.
> - Configurez les options de réplication de domaine et de mot de passe appropriées.
> - Cliquez sur "Suivant" et suivez les instructions pour terminer la promotion du serveur en tant que contrôleur de domaine.
>
> **3** **- Configuration supplémentaire de l'Active Directory :**
>
> - Après la promotion du serveur en tant que contrôleur de domaine, redémarrez le serveur.
> - Connectez-vous au serveur en utilisant les informations d'identification d'un compte d'administrateur de domaine.
> - Une fois connecté, vous pouvez commencer à effectuer des tâches de configuration supplémentaires, telles que la création d'utilisateurs, de groupes, de stratégies de groupe, etc., en utilisant les outils d'administration Active Directory tels que Utilisateurs et ordinateurs Active Directory, Utilisateurs et groupes locaux, Stratégie de groupe, etc.

## 10. Mise en place du DHCP et DNS :

- **Pour mettre en place un service DHCP et DNS sur un serveur Windows Server 2022, suivez les étapes suivantes :**

> **1 - Installation du rôle DHCP et DNS :**
>
> - Connectez-vous au serveur Windows Server 2022 en tant qu'administrateur.
> - Ouvrez le Gestionnaire de serveur en cliquant sur l'icône correspondante dans la barre des tâches ou en utilisant la fonction de recherche.
> - Dans le Gestionnaire de serveur, cliquez sur "Ajouter des rôles et des fonctionnalités".
> - Suivez les instructions de l'assistant d'installation pour sélectionner le serveur sur lequel vous souhaitez installer les rôles et les fonctionnalités.
> - Dans la liste des rôles disponibles, sélectionnez "Services de domaine Active Directory".
> - Acceptez les fonctionnalités supplémentaires recommandées pour le rôle DHCP et DNS et cliquez sur "Installer" pour démarrer l'installation.
>
> **2 - Configuration du service DHCP :**
>
> - Une fois l'installation du rôle DHCP terminée, ouvrez le Gestionnaire DHCP à partir du menu Démarrer.
> - Cliquez avec le bouton droit de la souris sur le nœud "Serveur DHCP" et sélectionnez "Ajouter un serveur DHCP".
> - Suivez les instructions à l'écran pour spécifier le nom ou l'adresse IP du serveur DHCP que vous souhaitez gérer.
> - Configurez les paramètres DHCP tels que les plages d'adresses IP, les options de location, les exclusions, les options de routage, etc., en fonction de vos besoins spécifiques.
> - Validez les paramètres de configuration et activez le service DHCP.
>
> **3 -Configuration du service DNS :**
>
> - Après l'installation du rôle DNS, ouvrez le Gestionnaire DNS à partir du menu Démarrer.
> - Dans le Gestionnaire DNS, cliquez avec le bouton droit de la souris sur le nœud "Serveur DNS" et sélectionnez "Configuration du serveur".
> - Configurez les zones DNS en fonction de vos besoins, tels que les zones de recherche directe, les zones de recherche inversée, les enregistrements de ressources, etc.
> - Ajoutez des enregistrements DNS tels que les enregistrements A, les enregistrements CNAME, les enregistrements MX, etc., pour faire correspondre les noms d'hôtes aux adresses IP.
> - Configurez les paramètres avancés du service DNS, tels que les mises à jour dynamiques, les transferts de zone, les redirections, etc., en fonction de vos besoins spécifiques.
>
> **4 - Vérification du service DHCP et DNS :**
>
> - Pour vérifier le fonctionnement du service DHCP, assurez-vous qu'il attribue correctement les adresses IP aux clients du réseau.
> - Pour vérifier le fonctionnement du service DNS, utilisez des outils de résolution de noms tels que "nslookup" pour résoudre les noms d'hôtes en adresses IP et vice versa.

## 11. Mise en place le partage de fichiers SMB :

- **Pour mettre en place le partage de fichiers SMB (Server Message Block) sur Windows Server 2022, vous devez suivre les étapes suivantes :**

> **Étape 1 : Installation du rôle de partage de fichiers**
>
> - Connectez-vous à votre serveur Windows Server 2022 en tant qu'administrateur.
> - Ouvrez le Gestionnaire de serveur en cliquant sur l'icône correspondante dans la barre des tâches ou en le recherchant dans le menu Démarrer.
> - Dans le Gestionnaire de serveur, cliquez sur "Ajouter des rôles et des fonctionnalités".
> - Suivez les étapes de l'Assistant "Ajouter des rôles et des fonctionnalités" jusqu'à atteindre la page "Sélection des rôles du serveur".
>   Cochez la case "Services de fichiers et de stockage" et cliquez sur "Suivant".
> - Sur la page "Sélection des fonctionnalités", vous pouvez choisir les fonctionnalités supplémentaires nécessaires pour votre environnement de partage de fichiers. Par exemple, vous pouvez sélectionner "Service de partage de fichiers SMB" pour activer SMB. Cliquez sur "Suivant".
>   Continuez à suivre les étapes de l'Assistant jusqu'à l'installation complète du rôle de partage de fichiers.
>
> **Étape 2 : Configuration du partage de fichiers**
>
> - Une fois l'installation terminée, ouvrez "Gestion de serveur" à partir du menu Démarrer.
>   Dans la fenêtre "Gestion de serveur", cliquez sur "Outils" dans la barre de menu supérieure, puis sélectionnez "Gestionnaire de partage et de stockage".
> - Dans le "Gestionnaire de partage et de stockage", vous pouvez créer de nouveaux partages de fichiers en cliquant avec le bouton droit de la souris sur "Partages de fichiers" et en sélectionnant "Nouveau partage de fichier".
> - Suivez les instructions de l'Assistant pour créer le partage de fichier en spécifiant le dossier à partager, les autorisations d'accès, les options de partage, etc.
> - Lorsque vous configurez les autorisations d'accès, assurez-vous de définir les autorisations appropriées pour les utilisateurs ou les groupes qui doivent accéder au partage de fichiers.
> - Une fois que vous avez créé le partage de fichiers, les utilisateurs de votre réseau pourront y accéder en utilisant l'adresse UNC (Universal Naming Convention) correspondante, par exemple "\nom-du-serveur\nom-du-partage".
>
> - Il est important de prendre en compte les considérations de sécurité appropriées lors de la configuration du partage de fichiers SMB, telles que la configuration des pare-feux, le chiffrement des communications SMB, l'utilisation de mots de passe forts, etc.
